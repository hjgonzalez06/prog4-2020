# Repositorio de Sesiones Prácticas - Programación IV 2020

Repositorio creado por el facilitador de la asignatura Programación IV de la carrera Ingeniería de Sistemas de la Universdad de Margarita, para subir códigos desarrollados en las sesiones prácticas.

### Tecnologías usadas en la asignatura:
    -HTML5.
    -CSS3.
    -PHP.
    -SQL.
    -JavaScript, jQuery & AJAX.
    -Frameworks.
    
#
                                 © 2020 Hiram J. González & Universidad de Margarita.
                              Todos los derechos reservados. Protegido bajo licencia MIT.
