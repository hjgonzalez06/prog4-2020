<?php

    require_once('../config/gestion.php');

    $consulta = new gestion();

    session_start();

    if($_GET['create']){

        if(isset($_POST['user'])&&isset($_POST['pass'])&&isset($_POST['fullname'])
           &&isset($_POST['email'])&&isset($_POST['address'])){

            if($_POST['user']!=""&&$_POST['pass']!=""&&$_POST['fullname']!=""
               &&$_POST['email']!=""&&$_POST['address']!=""){

                $usuario = $consulta->limpiar($_POST['user']);
                $contraseña = password_hash($consulta->limpiar($_POST['pass']),PASSWORD_DEFAULT);
                $nombre = $consulta->limpiar($_POST['fullname']);
                $correo = $consulta->limpiar($_POST['email']);
                $direccion = $consulta->limpiar($_POST['address']);

                $consulta->crearUsuario($usuario,$contraseña,$nombre,$correo,$direccion);

            }else{

                header ('Location: ../login/new.php?incomplete=true');

            }

        }else{

            header ('Location: ../login/new.php?incomplete=true');
    
        }

    }

    if($_GET['update']){

        if(isset($_POST['id'])&&isset($_POST['user'])&&isset($_POST['pass'])&&isset($_POST['fullname'])
           &&isset($_POST['email'])&&isset($_POST['address'])){

            if($_POST['id']!=""&&$_POST['user']!=""&&$_POST['pass']!=""&&$_POST['fullname']!=""
               &&$_POST['email']!=""&&$_POST['address']!=""){

                $id = $consulta->limpiar($_POST['id']);
                $usuario = $consulta->limpiar($_POST['user']);
                $contraseña = password_hash($consulta->limpiar($_POST['pass']),PASSWORD_DEFAULT);
                $nombre = $consulta->limpiar($_POST['fullname']);
                $correo = $consulta->limpiar($_POST['email']);
                $direccion = $consulta->limpiar($_POST['address']);

                $consulta->actualizarUsuario($id,$usuario,$contraseña,$nombre,$correo,$direccion);

            }else{

                header ('Location: ../forms/update.php?id='.$_POST["id"].'&imcomplete=true');

            }

        }else{

            header ('Location: ../forms/update.php?id='.$_POST["id"].'&imcomplete=true');

        }

    }else if($_GET['delete']){

        $consulta->eliminarUsuario($_GET['id']);

    }else{

        header ('Location: ../home/home.php');

    }