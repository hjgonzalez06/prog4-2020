<?php

    require_once('conexion.php');

    class gestion{

        protected $baseDatos;

        public function __construct(){

            $conexion = new conexion();

            $this->baseDatos = $conexion->__conexion();

        }

        public function visualizarUsuarios(){

            $query = "SELECT * FROM users";

            $resultado = $this->baseDatos->prepare($query);

            $resultado->execute();

            return $resultado->fetchAll();

        } 

        public function crearUsuario($usuario,$contraseña,$nombre,$correo,$direccion){

            $query = "INSERT INTO users (username,pass,fullname,email,address)
                      VALUES (:username,:pass,:fullname,:email,:address)";

            $resultado = $this->baseDatos->prepare($query);

            $resultado->execute(array(":username"=>$usuario,":pass"=>$contraseña,":fullname"=>$nombre,
                                      ":email"=>$correo,":address"=>$direccion));

            return header ('Location: ../login/login.php?success=true');

        }

        public function obtenerUsuario($id){

            $query = "SELECT * FROM users WHERE account_id = :account_id";

            $resultado = $this->baseDatos->prepare($query);

            $resultado->execute(array(":account_id"=>$id));

            return $resultado->fetch();

        }

        public function obtenerNombreUsuario($id){

            $query = "SELECT fullname FROM users WHERE account_id = :account_id";

            $resultado = $this->baseDatos->prepare($query);

            $resultado->execute(array(":account_id"=>$id));

            $nombre = $resultado->fetch();

            return $nombre['fullname'];

        }

        public function actualizarUsuario($id,$usuario,$contraseña,$nombre,$correo,$direccion){

            if($contraseña == ""){

                $query = "UPDATE users SET username = :username, fullname = :fullname,
                email = :email, address = :address WHERE account_id = :account_id";

                $resultado = $resultado = $this->baseDatos->prepare($query);

                $resultado->execute(array(":username"=>$usuario,":fullname"=>$nombre,
                                ":email"=>$correo,":address"=>$direccion,":account_id"=>$id));

            }else{

                $contraseña = password_hash($contraseña,PASSWORD_DEFAULT);

                $query = "UPDATE users SET username = :username, pass = :pass, fullname = :fullname,
                email = :email, address = :address WHERE account_id = :account_id";

                $resultado = $resultado = $this->baseDatos->prepare($query);

                $resultado->execute(array(":username"=>$usuario,":pass"=>$contraseña,":fullname"=>$nombre,
                                ":email"=>$correo,":address"=>$direccion,":account_id"=>$id));

            }

        }

        public function eliminarUsuario($id){

            $query = "DELETE FROM users WHERE account_id = :account_id";

            $resultado = $this->baseDatos->prepare($query);

            $resultado->execute(array(":account_id" => $id));

            return header('Location: ../home/home.php');

        }

        public function limpiar($input){

            return htmlentities(addslashes($input));

        }

    }