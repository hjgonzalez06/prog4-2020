<?php

    session_start();

    if(!isset($_SESSION['usuario']) || !isset($_SESSION['account_id'])){
        header('Location: ../login/login.php');
    }

    require_once("../config/gestion.php");

    $consulta = new gestion();

    $usuario = $consulta->obtenerUsuario($_POST['id']);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/forms-style.css">
</head>
<body>

    <div>

        <h1 id="main-title" class="col-6">Actualización de Usuario</h1>

        <form method="post" autocomplete="off" role="form">
            <input type="text" id="id" name="id" value="<?php echo $usuario['account_id']; ?>" hidden="true">
            <div class="form-group">
                <input type="text" id="user" name="user" value="<?php echo $usuario['username'] ?>">
                <input type="text" id="fullname" name="fullname" value="<?php echo $usuario['fullname'] ?>">
                <input type="email" id="email" name="email" value="<?php echo $usuario['email'] ?>">
            </div>
            <div class="form-group">
                <input type="text" id="address" name="address" value="<?php echo $usuario['address'] ?>">
                <select id="pass" name="pass" class="form-control">
                    <option value="">Restablecer contraseña</option>
                    <option value="user1234">Default</option>
                </select>
            </div>
            <div class="opciones">
                <button type="button" id="btn-update" name="submit">Actualizar</button>
                <button type="button" id="btn-home" name="submit">Regresar</button>
            </div>
        </form>

    </div>
    
</body>
</html>