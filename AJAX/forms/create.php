<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse - Programación IV</title>
    <link rel="shortcut icon" href="../assets/logos/logo-p4.png" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/forms-style.css">
</head>
<body>

    <div>

        <h1 id="main-title" class="col-6">Registro de Usuario</h1>

        <form id="create" method="post" autocomplete="off" role="form">
            
            <div class="form-group">
                <input type="text" id="fullname" name="fullname" placeholder="Introduzca su nombre y apellido">
                <input type="email" id="email" name="email" placeholder="Introduzca su correo electrónico">
                <input type="text" id="address" name="address" placeholder="Introduzca su dirección">
            </div>
            <div class="form-group">
                <input type="text" id="user" name="user" placeholder="Introduzca un nombre de usuario">
                <input type="password" id="pass" name="pass" placeholder="Introduzca su contraseña">
            </div>
            <div class="opciones">
                <button type="button" id="btn-create" name="submit">Registrarse</button>
                <button type="button" id="btn-login" name="submit">Regresar</button>
            </div>

        </form>

    </div>
    
</body>
</html>