<?php

    require_once('../config/check.php');

    $verificar = new check();

    session_start();

    if($verificar->login()){

        $_SESSION['usuario'] = $verificar->getUsuario();
        $_SESSION['account_id'] = $verificar->getAccountId();

        header('Location: ../home/home.php');

    }else{

        $_SESSION['error'] = true;

        header('Location: login.php');

    }