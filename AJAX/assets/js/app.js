$(document).ready(function(){

    $('#create').on('click',function (e){

        e.preventDefault();

        $.ajax({
            url: ('../forms/create.php'),
            method: 'post',
            data: {}
        })
        .done(function(output){
            $('#login').hide();
            $('#forms').empty().append(output);
        })
        .fail(function () {
            alert('¡Algo ha ido mal!');
        })
        .always(function(){
            console.log('Completdo');
        });
        
    });

    $('#forms').on('click','#btn-create',function(e){

        e.preventDefault();

        $.ajax({
            url: '../actions/actions.php?create=true',
            method: 'post',
            data: { //$('#create').serialize();
                user: $('#user').val(),
                fullname: $('#fullname').val(),
                email: $('#email').val(),
                address: $('#address').val(),
                pass: $('#pass').val()
            },
            beforeSend: function(){
                var next = confirm('¿Acepta contnuar con el registro?');
                if(!next){
                    $('#forms').empty();
                    $('#login').show();
                    return false;
                }
            }
        })
        .done(function () {
            $('#forms').empty();
            $('#login').show();
            alert('Su cuenta ha sido creada exitosamente.');
        })
        .fail(function(e,er){
            if(er == "canceled"){
                alert('Registro cancelado.');
            }else{
                alert('¡Algo ha ido mal!');
            }
        })
        .always(function(){
            console.log('Completdo');
        });

    });

    $('#forms').on('click','#btn-login',function(e){

        e.preventDefault();

        $('#forms').empty();
        $('#login').show();

    });

    $('.update').on('click',function(e){

        e.preventDefault();

        let id = $(this).data('id');

        $.ajax({
            url: ' ../forms/update.php',
            method: 'post',
            data: {
                id: id
            }
        })
        .done(function(output){
            $('#main-title, #usuarios').hide();
            $('#forms').empty().append(output);
        })
        .fail(function () {
            alert('¡Algo ha ido mal!');
        })
        .always(function(){
            console.log('Completdo');
        });

    });

    $('#forms').on('click','#btn-update',function(e){

        e.preventDefault();

        $.ajax({
            url: '../actions/actions.php?update=true',
            method: 'post',
            data: {
                id: $('#id').val(),
                user: $('#user').val(),
                fullname: $('#fullname').val(),
                email: $('#email').val(),
                address: $('#address').val(),
                pass: $('#pass').val()
            },
            beforeSend: function(){
                var next = confirm('¿Está seguro de continuar con la actualización?');
                if(!next){
                    $('#forms').empty();
                    $('#main-title, #usuarios').show();
                    return false;
                }
            }
        })
        .done(function () {
            $('#forms').empty();
            $('#main-title, #usuarios').show();
            alert('Actualización exitosa.');
        })
        .fail(function(e,er){
            if(er == "canceled"){
                alert('Actualización cancelada.');
            }else{
                alert('¡Algo ha ido mal!');
            }
        })
        .always(function(){
            console.log('Completado');
        });

    });

    $('#forms').on('click','#btn-home',function(e){

        e.preventDefault();

        $('#forms').empty();
        $('#main-title, #usuarios').show();

    });

    $('.delete').on('click',function(e){

        e.preventDefault();

        let id = $(this).data('id');

        $.ajax({
            url: '../../actions/actions.php?delete=true',
            method: 'post',
            data: {
                id: id
            },
            beforeSend: function(){
                var next = confirm('¿Está seguro de continuar con la eliminación?');
                if(!next){
                    return false;
                }
            }
        })
        .done(function () {
            alert('Eliminación exitosa.');
        })
        .fail(function(e,er){
            if(er == "canceled"){
                alert('Eliminación cancelada.');
            }else{
                alert('¡Algo ha ido mal!');
            }
        })
        .always(function(){
            console.log('Completado');
        });

    })

});